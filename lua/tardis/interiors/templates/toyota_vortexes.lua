TARDIS:AddInteriorTemplate("toyota_vortex_2013", {
	Exterior = {
		Parts = {
			vortex={
				model="models/doctorwho1200/toyota/2013timevortex.mdl",
				pos=Vector(0,0,0),
				ang=Angle(0,180,0),
				scale=10
			}
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_vortex_2013B", {
	Exterior = {
		Parts = {
			vortex={
				model="models/doctorwho1200/toyota/2013timevortex.mdl",
				pos=Vector(0,0,0),
				ang=Angle(0,0,0),
				scale=10
			}
		},
	},
})

TARDIS:AddInteriorTemplate("toyota_vortex_2014", {
	Exterior = {
		Parts = {
			vortex={
				model="models/doctorwho1200/toyota/2014timevortex.mdl",
				pos=Vector(0,0,50),
				ang=Angle(0,0,0),
				scale=10
			}
		},
	},
})