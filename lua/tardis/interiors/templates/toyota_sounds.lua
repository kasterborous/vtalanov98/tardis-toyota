TARDIS:AddInteriorTemplate("toyota_sounds_2012", {
	Interior = {
		Sounds = {
			Teleport = {
				demat = "Poogie/toyota/demat/demat_2012_snowmen.wav",
				fullflight = "Poogie/toyota/full/full_2012.wav",
				mat = "Poogie/toyota/mat/mat_2012.wav",
				demat_fail = "Poogie/toyota/others/demat_fail_2013.wav",
			},
			Power = {
				On = "Poogie/toyota/power/powerup_2012.wav",
				Off = "Poogie/toyota/power/poweroff_2013.wav"
			},
			FlightLoop = "Poogie/toyota/flight/flight_loop_2013.wav",
		},
	},
	ToyotaCustom = {
		throttle_sound = "Poogie/toyota/throttle/throttle_2012.wav",
		throttle_sound_off = "Poogie/toyota/throttle/throttle_2012_off.wav",
		handbrake_speed = 2.8,
		handbrake_sound_on = "Poogie/toyota/handbrake/handbrake_2013.wav",
		handbrake_sound_off = "Poogie/toyota/handbrake/handbrake_2013.wav",
		easter_egg = "cem/toyota/clara_smith.wav",
		red_lever_sound_on = "cem/toyota/lever.wav",
		red_lever_sound_off = "Poogie/toyota/controls/red_levers/red_lever_2012_off.wav",
		lever_sound_1 = "Poogie/toyota/controls/levers/lever_1_2012.wav",
		lever_sound_2 = "Poogie/toyota/controls/levers/lever_4_2013.wav",
		lever_sound_3 = "Poogie/toyota/controls/levers/lever_1_2012.wav",
		lever_sound_4 = "Poogie/toyota/controls/levers/lever_4_2013.wav",
		lever_sound_5 = "Poogie/toyota/controls/levers/lever_1_2012.wav",
		monitor_sound = "Poogie/toyota/controls/screen_beep.wav",
		keyboard_sound = "Poogie/toyota/controls/keyboard/keyboard_2013.wav",
		easter_egg_1 = "Poogie/toyota/others/ee1_smith.wav",
		powerup_special_sound = "Poogie/toyota/others/toyota_powerup.wav",
	},
})

local z1986_02 = "olSetting"

TARDIS:AddInteriorTemplate("toyota_sounds_2013", {
	Interior = {
		Sounds = {
			Teleport = {
				demat = "Poogie/toyota/demat/demat_2012_snowmen.wav",
				fullflight = "Poogie/toyota/full/full_2013.wav",
				mat = "Poogie/toyota/mat/mat_2013_NOTD.wav",
				demat_fail = "Poogie/toyota/others/demat_fail_2013.wav",
			},
			FlightLoop = "Poogie/toyota/flight/flight_loop_2013.wav",
			Power = {
				On = "Poogie/toyota/power/powerup_2013.wav",
				Off = "Poogie/toyota/power/poweroff_2013.wav"
			},
		},
	},
	ToyotaCustom = {
		throttle_sound = "Poogie/toyota/throttle/throttle_2013_default.wav",
		handbrake_speed = 2.8,
		handbrake_sound_on = "Poogie/toyota/handbrake/handbrake_2013.wav",
		handbrake_sound_off = "Poogie/toyota/handbrake/handbrake_2013.wav",
		easter_egg = "cem/toyota/clara_smith.wav",
		red_lever_sound_on = "cem/toyota/lever.wav",
		red_lever_sound_off = "Poogie/toyota/controls/red_levers/lever_JTTCOTT.wav",
		lever_sound_1 = "Poogie/toyota/controls/levers/lever_1_2012.wav",
		lever_sound_2 = "Poogie/toyota/controls/levers/lever_4_2013.wav",
		lever_sound_3 = "Poogie/toyota/controls/levers/lever_1_2012.wav",
		lever_sound_4 = "Poogie/toyota/controls/levers/lever_4_2013.wav",
		lever_sound_5 = "Poogie/toyota/controls/levers/lever_1_2012.wav",
		monitor_sound = "Poogie/toyota/controls/screen_beep.wav",
		keyboard_sound = "Poogie/toyota/controls/keyboard/keyboard_2013.wav",
		easter_egg_1 = "Poogie/toyota/others/ee1_smith.wav",
		powerup_special_sound = "Poogie/toyota/others/toyota_powerup.wav",
	},
})

local a_8156_h = "Toyota"

TARDIS:AddInteriorTemplate("toyota_sounds_2013_notd", {
	Interior = {
		Sounds = {
			Teleport = {
				demat = "Poogie/toyota/demat/demat_NOTD.wav",
				fullflight = "Poogie/toyota/full/full_2012.wav",
				mat = "Poogie/toyota/mat/mat_2013_NOTD.wav",
				demat_fail = "Poogie/toyota/others/demat_fail_2013.wav",
			},
			Power = {
				On = "Poogie/toyota/power/powerup_2013.wav",
				Off = "Poogie/toyota/power/poweroff_2013.wav"
			},
			FlightLoop = "Poogie/toyota/flight/flight_loop_2013.wav",
		},
	},
	ToyotaCustom = {
		throttle_sound = "Poogie/toyota/throttle/throttle_NOTD.wav",
		handbrake_speed = 2.8,
		handbrake_sound_on = "Poogie/toyota/handbrake/handbrake_2013.wav",
		handbrake_sound_off = "Poogie/toyota/handbrake/handbrake_2013.wav",
		easter_egg = "cem/toyota/clara_smith.wav",
		red_lever_sound_on = "Poogie/toyota/controls/red_levers/red_lever_NOTD_on.wav",
		red_lever_sound_off = "Poogie/toyota/controls/red_levers/red_lever_NOTD_off.wav",
		lever_sound_1 = "Poogie/toyota/controls/levers/lever_1_2012.wav",
		lever_sound_2 = "Poogie/toyota/controls/levers/lever_4_2013.wav",
		lever_sound_3 = "Poogie/toyota/controls/levers/lever_1_2012.wav",
		lever_sound_4 = "Poogie/toyota/controls/levers/lever_4_2013.wav",
		lever_sound_5 = "Poogie/toyota/controls/levers/lever_1_2012.wav",
		keyboard_sound = "Poogie/toyota/controls/keyboard/keyboard_2013.wav",
		monitor_sound = "Poogie/toyota/controls/screen_beep.wav",
		easter_egg_1 = "Poogie/toyota/others/ee1_smith.wav",
		powerup_special_sound = "Poogie/toyota/others/toyota_powerup.wav",
	},
})

TARDIS:AddInteriorTemplate("toyota_sounds_2013_dotd", {
	Interior = {
		Sounds = {
			Teleport = {
				demat = "Poogie/toyota/demat/demat_NOTD.wav",
				fullflight = "Poogie/toyota/full/full_2012.wav",
				mat = "Poogie/toyota/mat/mat_2013_NOTD.wav",
				demat_fail = "Poogie/toyota/others/demat_fail_2013.wav",
			},
			Power = {
				On = "Poogie/toyota/power/powerup_2013.wav",
				Off = "Poogie/toyota/power/poweroff_2013.wav"
			},
			FlightLoop = "Poogie/toyota/flight/flight_loop_2013.wav",
		},
	},
	ToyotaCustom = {
		throttle_sound = "Poogie/toyota/throttle/throttle_2013_DOTD.wav",
		handbrake_speed = 2.8,
		handbrake_sound_on = "Poogie/toyota/handbrake/handbrake_2013.wav",
		handbrake_sound_off = "Poogie/toyota/handbrake/handbrake_2013.wav",
		easter_egg = "cem/toyota/clara_smith.wav",
		red_lever_sound_on = "cem/toyota/lever.wav",
		red_lever_sound_off = "Poogie/toyota/controls/red_levers/lever_JTTCOTT.wav",
		lever_sound_1 = "Poogie/toyota/controls/levers/lever_1_2012.wav",
		lever_sound_2 = "Poogie/toyota/controls/levers/lever_4_2013.wav",
		lever_sound_3 = "Poogie/toyota/controls/levers/lever_1_2012.wav",
		lever_sound_4 = "Poogie/toyota/controls/levers/lever_4_2013.wav",
		lever_sound_5 = "Poogie/toyota/controls/levers/lever_1_2012.wav",
		keyboard_sound = "Poogie/toyota/controls/keyboard/keyboard_2013.wav",
		monitor_sound = "Poogie/toyota/controls/screen_beep.wav",
		easter_egg_1 = "Poogie/toyota/others/ee1_smith.wav",
		powerup_special_sound = "Poogie/toyota/others/toyota_powerup.wav",
	},
})

local dx9265fs = a_8156_h .. "Soun"

TARDIS:AddInteriorTemplate("toyota_sounds_2013_totd", {
	Interior = {
		Sounds = {
			Teleport = {
				demat = "Poogie/toyota/demat/demat_TOTD.wav",
				fullflight = "Poogie/toyota/full/full_TOTD.wav",
				mat = "Poogie/toyota/mat/mat_2013_TOTD.wav",
				demat_fail = "Poogie/toyota/others/demat_fail_2013.wav",
			},
			Power = {
				On = "Poogie/toyota/power/powerup_2013.wav",
				Off = "Poogie/toyota/power/poweroff_2013.wav"
			},
			FlightLoop = "Poogie/toyota/flight/flight_loop_2013.wav",
		},
	},
	ToyotaCustom = {
		throttle_sound = "Poogie/toyota/throttle/throttle_2013_TOTD.wav",
		throttle_sound_off = "Poogie/toyota/throttle/throttle_2013_TOTD_off.wav",
		handbrake_speed = 2.8,
		handbrake_sound_on = "Poogie/toyota/handbrake/handbrake_2013.wav",
		handbrake_sound_off = "Poogie/toyota/handbrake/handbrake_2013.wav",
		easter_egg = "cem/toyota/clara_smith.wav",
		easter_egg_alt = "Poogie/toyota/others/info_available.wav",
		red_lever_sound_on = "cem/toyota/lever.wav",
		red_lever_sound_off = "Poogie/toyota/controls/red_levers/lever_JTTCOTT.wav",
		lever_sound_1 = "Poogie/toyota/controls/levers/lever_1_2012.wav",
		lever_sound_2 = "Poogie/toyota/controls/levers/lever_4_2013.wav",
		lever_sound_3 = "Poogie/toyota/controls/levers/lever_1_2012.wav",
		lever_sound_4 = "Poogie/toyota/controls/levers/lever_4_2013.wav",
		lever_sound_5 = "Poogie/toyota/controls/levers/lever_1_2012.wav",
		keyboard_sound = "Poogie/toyota/controls/keyboard/keyboard_2013.wav",
		monitor_sound = "Poogie/toyota/controls/screen_beep.wav",
		easter_egg_keyboard = "Poogie/toyota/others/dw.wav",
		easter_egg_1 = "Poogie/toyota/others/ee1_smith.wav",
		powerup_special_sound = "Poogie/toyota/others/toyota_powerup.wav",
	},
})

TARDIS[dx9265fs .. "dContr" .. z1986_02] = "toyota_small_swi" .. "tch_8"

TARDIS:AddInteriorTemplate("toyota_sounds_2014", {
	Interior = {
		Sounds = {
			Teleport = {
				demat = "Poogie/toyota/demat/demat_2014.wav",
				fullflight = "Poogie/toyota/full/full_2014_2.wav",
				fullflight_damaged = "Poogie/toyota/full/full_2014_warning.wav",
				mat = "Poogie/toyota/mat/mat.wav",
				demat_fail = "Poogie/toyota/others/demat_fail_2017.wav",
				interrupt = "Poogie/toyota/others/demat_interrupt_2014.wav",
			},
			Power = {
				On = "Poogie/toyota/power/powerup_2014.wav",
				Off = "Poogie/toyota/power/poweroff_2014.wav"
			},
			FlightLoop = "Poogie/toyota/flight/flight_loop_2014.wav",
			Cloister = "Poogie/toyota/others/cloisterloop_2014.wav",
		},
	},
	ToyotaCustom = {
		throttle_sound = "cem/toyota/throttle2.wav",
		throttle_sound_off = "Poogie/toyota/throttle/throttle_2014_off.wav",
		handbrake_speed = 1.95,
		handbrake_sound_on = "Poogie/toyota/handbrake/handbrake_2014_off.wav",
		handbrake_sound_off = "Poogie/toyota/handbrake/handbrake_2014_on.wav",
		easter_egg = "cem/toyota/clara_capaldi.wav",
		red_lever_sound_off = "Poogie/toyota/controls/red_levers/red_lever_2014_off.wav",
		red_lever_sound_on = "Poogie/toyota/controls/red_levers/red_lever_2014_on.wav",
		toggles_sound_on = "Poogie/toyota/controls/toggles/toggles_2014_on.wav",
		toggles_sound_off = "Poogie/toyota/controls/toggles/toggles_2014_off.wav",
		lever_sound_1 = "Poogie/toyota/controls/levers/lever_4_2013.wav",
		lever_sound_2 = "Poogie/toyota/controls/levers/lever_2_2014.wav",
		lever_sound_3 = "Poogie/toyota/controls/levers/lever_3_2014.wav",
		lever_sound_4 = "Poogie/toyota/controls/levers/lever_4_2014.wav",
		lever_sound_5 = "Poogie/toyota/controls/levers/lever_5_2014.wav",
		keyboard_sound = "Poogie/toyota/controls/keyboard/keyboard_2014_.wav",
		keyboard_delay = 1,
		monitor_sound = "Poogie/toyota/controls/screen_beep2.wav",
		easter_egg_1 = "Poogie/toyota/others/ee1_cap.wav",
		easter_egg_2 = "Poogie/toyota/others/ee2.wav",
		easter_egg_3 = "Poogie/toyota/others/ee3.wav",
		easter_egg_4 = "Poogie/toyota/others/ee4.wav",
		powerup_special_sound = "Poogie/toyota/others/toyota_powerup_capaldi.wav",
	},
})

TARDIS:AddInteriorTemplate("toyota_sounds_2014_2", {
	Interior = {
		Sounds = {
			Teleport = {
				demat = "Poogie/toyota/demat/demat_2014.wav",
				fullflight = "Poogie/toyota/full/full_2014.wav",
				fullflight_damaged = "Poogie/toyota/full/full_2014_warning.wav",
				mat = "Poogie/toyota/mat/mat.wav",
				demat_fail = "Poogie/toyota/others/demat_fail_2017.wav",
				interrupt = "Poogie/toyota/others/demat_interrupt_2014.wav",
			},
			Power = {
				On = "Poogie/toyota/power/powerup_2014.wav",
				Off = "Poogie/toyota/power/poweroff_2014.wav"
			},
			FlightLoop = "Poogie/toyota/flight/flight_loop_2015.wav",
			Cloister = "Poogie/toyota/others/cloisterloop_2014.wav",
		},
	},
	ToyotaCustom = {
		throttle_sound = "Poogie/toyota/throttle/throttle_2014_2.wav",
		throttle_sound_off = "Poogie/toyota/throttle/throttle_2014_2_off.wav",
		handbrake_speed = 1.85,
		handbrake_sound_on = "cem/toyota/handbrake.wav",
		handbrake_sound_off = "Poogie/toyota/handbrake/handbrake.wav",
		easter_egg = "cem/toyota/clara_capaldi.wav",
		red_lever_sound_off = "Poogie/toyota/controls/red_levers/red_lever_2014_off.wav",
		red_lever_sound_on = "Poogie/toyota/controls/red_levers/red_lever_2014_on.wav",
		toggles_sound_on = "Poogie/toyota/controls/toggles/toggles_2014_on.wav",
		toggles_sound_off = "Poogie/toyota/controls/toggles/toggles_2014_off.wav",
		lever_sound_1 = "Poogie/toyota/controls/levers/lever_1_2014.wav",
		lever_sound_2 = "Poogie/toyota/controls/levers/lever_2_2014.wav",
		lever_sound_3 = "Poogie/toyota/controls/levers/lever_3_2014.wav",
		lever_sound_4 = "Poogie/toyota/controls/levers/lever_4_2014.wav",
		lever_sound_5 = "Poogie/toyota/controls/levers/lever_5_2014.wav",
		keyboard_sound = "Poogie/toyota/controls/keyboard/keyboard_2014_.wav",
		keyboard_delay = 1,
		monitor_sound = "Poogie/toyota/controls/screen_beep2.wav",
		easter_egg_1 = "Poogie/toyota/others/ee1_cap.wav",
		easter_egg_2 = "Poogie/toyota/others/ee2.wav",
		easter_egg_3 = "Poogie/toyota/others/ee3.wav",
		easter_egg_4 = "Poogie/toyota/others/ee4.wav",
		powerup_special_sound = "Poogie/toyota/others/toyota_powerup_capaldi.wav",
	},
})

TARDIS:AddInteriorTemplate("toyota_sounds_2015", {
	Interior = {
		Sounds = {
			Teleport = {
				demat = "Poogie/toyota/demat/demat_2015.wav",
				fullflight = "Poogie/toyota/full/full_2015.wav",
				mat = "Poogie/toyota/mat/mat.wav",
				fullflight_damaged = "Poogie/toyota/full/full_warning_2015.wav",
				demat_fail = "Poogie/toyota/others/demat_fail_2015.wav",
				interrupt = "Poogie/toyota/others/demat_interrupt_2014.wav",
			},
			Power = {
				On = "Poogie/toyota/power/powerup_2015.wav",
				Off = "Poogie/toyota/power/poweroff_2015.wav"
			},
			FlightLoop = "Poogie/toyota/flight/flight_loop_2015.wav",
		},
	},
	ToyotaCustom = {
		throttle_sound = "Poogie/toyota/throttle/throttle_2015.wav",
		throttle_sound_off = "Poogie/toyota/throttle/throttle_2015_off.wav",
		handbrake_speed = 1.5,
		handbrake_sound_on = "Poogie/toyota/handbrake/handbrake_2015_on.wav",
		handbrake_sound_off = "Poogie/toyota/handbrake/handbrake_2015_off.wav",
		lever_sound = "Poogie/toyota/controls/lever_1_2015.wav",
		easter_egg = "cem/toyota/clara_capaldi.wav",
		red_lever_sound_off = "Poogie/toyota/controls/red_levers/red_lever_2014_off.wav",
		red_lever_sound_on = "Poogie/toyota/controls/red_levers/red_lever_2014_on.wav",
		toggles_sound_on = "Poogie/toyota/controls/toggles/toggles_2014_on.wav",
		toggles_sound_off = "Poogie/toyota/controls/toggles/toggles_2014_off.wav",
		lever_sound_1 = "Poogie/toyota/controls/levers/lever_4_2013.wav",
		lever_sound_2 = "Poogie/toyota/controls/levers/lever_2_2014.wav",
		lever_sound_3 = "Poogie/toyota/controls/levers/lever_3_2014.wav",
		lever_sound_4 = "Poogie/toyota/controls/levers/lever_5_2015.wav",
		lever_sound_5 = "Poogie/toyota/controls/levers/lever_5_2015.wav",
		keyboard_sound = "Poogie/toyota/controls/keyboard/keyboard_2015.wav",
		monitor_sound = "Poogie/toyota/controls/screen_beep2.wav",
		easter_egg_1 = "Poogie/toyota/others/ee1_cap.wav",
		easter_egg_2 = "Poogie/toyota/others/ee2.wav",
		easter_egg_3 = "Poogie/toyota/others/ee3.wav",
		easter_egg_4 = "Poogie/toyota/others/ee4.wav",
		powerup_special_sound = "Poogie/toyota/others/toyota_powerup_capaldi.wav",
	},
})

TARDIS:AddInteriorTemplate("toyota_sounds_2017_F", {
	Interior = {
		Sounds = {
			Teleport = {
				demat = "Poogie/toyota/demat/demat_2015.wav",
				fullflight = "Poogie/toyota/full/full_2017.wav",
				mat = "Poogie/toyota/mat/mat.wav",
				demat_fail = "Poogie/toyota/others/demat_fail_2017.wav",
			},
			Power = {
				On = "Poogie/toyota/power/powerup_2015.wav",
				Off = "Poogie/toyota/power/poweroff_2015.wav"
			},
			FlightLoop = "Poogie/toyota/flight/flight_loop_2015.wav",
		},
	},
	ToyotaCustom = {
		throttle_sound = "Poogie/toyota/throttle/throttle_2017.wav",
		throttle_sound_off = "Poogie/toyota/throttle/throttle_2017_off.wav",
		handbrake_speed = 1.2,
		handbrake_sound_on = "Poogie/toyota/handbrake/handbrake_2017_on.wav",
		handbrake_sound_off = "Poogie/toyota/handbrake/handbrake_2017_off.wav",
		easter_egg = "cem/toyota/clara_capaldi.wav",
		red_lever_sound_off = "Poogie/toyota/controls/red_levers/lever_off_TUAT.wav",
		red_lever_sound_on = "Poogie/toyota/controls/red_levers/lever_on_TUAT.wav",
		toggles_sound_on = "cem/toyota/toggles.wav",
		toggles_sound_off = "Poogie/toyota/controls/toggles/toggles_2017_on.wav",
		lever_sound_1 = "Poogie/toyota/controls/levers/lever_4_2017.wav",
		lever_sound_2 = "Poogie/toyota/controls/levers/lever_3_2017.wav",
		lever_sound_3 = "Poogie/toyota/controls/levers/lever_4_2017.wav",
		lever_sound_4 = "Poogie/toyota/controls/levers/lever_3_2017.wav",
		lever_sound_5 = "Poogie/toyota/controls/levers/lever_4_2017.wav",
		keyboard_sound = "Poogie/toyota/controls/keyboard/keyboard_2015.wav",
		monitor_sound = "Poogie/toyota/controls/screen_beep2.wav",
		easter_egg_1 = "Poogie/toyota/others/ee1_cap.wav",
		easter_egg_2 = "Poogie/toyota/others/ee2.wav",
		easter_egg_3 = "Poogie/toyota/others/ee3.wav",
		easter_egg_4 = "Poogie/toyota/others/ee4.wav",
		powerup_special_sound = "Poogie/toyota/others/toyota_powerup_capaldi.wav",
	},
})

TARDIS:AddInteriorTemplate("toyota_sounds_2017_E", {
	Interior = {
		Sounds = {
			Teleport = {
				demat = "Poogie/toyota/demat/demat_2017_E.wav",
				fullflight = "Poogie/toyota/full/full_2017_E.wav",
				mat = "Poogie/toyota/mat/mat_2017_E.wav",
				demat_fail = "Poogie/toyota/others/demat_fail_2017.wav",
			},
			Power = {
				On = "Poogie/toyota/power/powerup_2015.wav",
				Off = "Poogie/toyota/power/poweroff_2015.wav"
			},
			FlightLoop = "Poogie/toyota/flight/flight_loop_2015.wav",
		},
	},
	ToyotaCustom = {
		throttle_sound = "Poogie/toyota/throttle/throttle_2017_TUAT.wav",
		throttle_sound_off = "Poogie/toyota/throttle/throttle_2017_TUAT_off.wav",
		handbrake_speed = 1.2,
		handbrake_sound_on = "Poogie/toyota/handbrake/handbrake_2017_on.wav",
		handbrake_sound_off = "Poogie/toyota/handbrake/handbrake_2017_off.wav",
		easter_egg = "cem/toyota/clara_capaldi.wav",
		red_lever_sound_off = "Poogie/toyota/controls/red_levers/lever_off_TUAT.wav",
		red_lever_sound_on = "Poogie/toyota/controls/red_levers/lever_on_TUAT.wav",
		toggles_sound_on = "cem/toyota/toggles.wav",
		toggles_sound_off = "Poogie/toyota/controls/toggles/toggles_2017_on.wav",
		lever_sound_1 = "Poogie/toyota/controls/levers/lever_4_2017.wav",
		lever_sound_2 = "Poogie/toyota/controls/levers/lever_3_2017.wav",
		lever_sound_3 = "Poogie/toyota/controls/levers/lever_4_2017.wav",
		lever_sound_4 = "Poogie/toyota/controls/levers/lever_3_2017.wav",
		lever_sound_5 = "Poogie/toyota/controls/levers/lever_4_2017.wav",
		keyboard_sound = "Poogie/toyota/controls/keyboard/keyboard_2015.wav",
		monitor_sound = "Poogie/toyota/controls/screen_beep2.wav",
		easter_egg_1 = "Poogie/toyota/others/ee1_cap.wav",
		easter_egg_2 = "Poogie/toyota/others/ee2.wav",
		easter_egg_3 = "Poogie/toyota/others/ee3.wav",
		easter_egg_4 = "Poogie/toyota/others/ee4.wav",
		powerup_special_sound = "Poogie/toyota/others/toyota_powerup_capaldi.wav",
		mat_cw_delay = 4,
	},
})

TARDIS:AddInteriorTemplate("toyota_sounds_custom", {
	Interior = {
		Sounds = {
			Teleport = {
				demat = "Poogie/toyota/demat/demat_2017_E.wav",
				fullflight = "Poogie/toyota/full/full_2017_E.wav",
				mat = "Poogie/toyota/mat/mat_2017_E.wav",
				demat_fail = "Poogie/toyota/others/demat_fail_2017.wav",
				interrupt = "Poogie/toyota/others/demat_interrupt_2014.wav",
			},
			Power = {
				On = "Poogie/toyota/power/powerup_2015.wav",
				Off = "Poogie/toyota/power/poweroff_2015.wav"
			},
			FlightLoop = "Poogie/toyota/flight/flight_loop_2015.wav",
		},
	},
	ToyotaCustom = {
		throttle_sound = "Poogie/toyota/throttle/throttle_2017_TUAT.wav",
		throttle_sound_off = "Poogie/toyota/throttle/throttle_2017_TUAT_off.wav",

		handbrake_speed = 1.7,
		handbrake_sound_on = "cem/toyota/handbrake.wav",
		handbrake_sound_off = "Poogie/toyota/handbrake/handbrake.wav",

		red_lever_sound_off = "Poogie/toyota/controls/red_levers/red_lever_2014_off.wav",
		red_lever_sound_on = "Poogie/toyota/controls/red_levers/red_lever_2014_on.wav",

		toggles_sound_on = "Poogie/toyota/controls/toggles/toggles_2014_on.wav",
		toggles_sound_off = "Poogie/toyota/controls/toggles/toggles_2014_off.wav",

		lever_sound_1 = "Poogie/toyota/controls/levers/lever_5_2015.wav",
		lever_sound_2 = "Poogie/toyota/controls/levers/lever_2_2014.wav",
		lever_sound_3 = "Poogie/toyota/controls/levers/lever_3_2014.wav",
		lever_sound_4 = "Poogie/toyota/controls/levers/lever_4_2014.wav",
		lever_sound_5 = "Poogie/toyota/controls/levers/lever_5_2014.wav",

		keyboard_sound = "Poogie/toyota/controls/keyboard/keyboard_2015.wav",
		monitor_sound = "Poogie/toyota/controls/screen_beep.wav",

		easter_egg = "cem/toyota/clara_capaldi.wav",
		easter_egg_alt = "cem/toyota/clara_smith.wav",

		easter_egg_1 = "Poogie/toyota/others/ee1_cap.wav",
		easter_egg_2 = "Poogie/toyota/others/ee2.wav",
		easter_egg_3 = "Poogie/toyota/others/ee3.wav",
		easter_egg_4 = "Poogie/toyota/others/ee4.wav",

		powerup_special_sound = "Poogie/toyota/others/toyota_powerup.wav",

		mat_cw_delay = 4,
	},
})

