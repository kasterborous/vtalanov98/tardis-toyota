local PART={}
PART.ID = "toyota_levers"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/levers.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.Sound = "cem/toyota/levers.wav"

TARDIS:AddPart(PART)