local PART={}
PART.ID = "toyota_lever3"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/lever3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.SoundOn ="Poogie/toyota/controls/slider_2_on.wav"
PART.SoundOff ="Poogie/toyota/controls/slider_off.wav"

TARDIS:AddPart(PART)