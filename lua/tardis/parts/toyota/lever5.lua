local PART={}
PART.ID = "toyota_lever5"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/lever5.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "cem/toyota/lever5.wav"

TARDIS:AddPart(PART)