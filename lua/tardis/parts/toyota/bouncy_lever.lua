local PART={}
PART.ID = "toyota_bouncy_lever"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/bouncy_lever.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "cem/toyota/levers.wav"

TARDIS:AddPart(PART)