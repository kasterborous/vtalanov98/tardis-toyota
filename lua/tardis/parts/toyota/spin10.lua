local PART={}
PART.ID = "toyota_spin10"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/spin10.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "cem/toyota/crank2.wav"

TARDIS:AddPart(PART)