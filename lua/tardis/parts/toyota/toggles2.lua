local PART={}
PART.ID = "toyota_toggles2"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/toggles2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1
PART.Sound = "Poogie/toyota/controls/toggles2.wav"

TARDIS:AddPart(PART)