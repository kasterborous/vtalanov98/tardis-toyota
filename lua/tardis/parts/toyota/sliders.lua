local PART={}
PART.ID = "toyota_sliders"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/sliders.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1
PART.Sound = "cem/toyota/sliders.wav"

TARDIS:AddPart(PART)