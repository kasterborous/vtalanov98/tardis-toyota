local PART={}
PART.ID = "toyota_ducks"
PART.Name = PART.ID
PART.Model = "models/cem/toyota/controls/ducks.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1
PART.SoundOn = "Poogie/toyota/controls/ducks_on.wav"
PART.SoundOff = "Poogie/toyota/controls/ducks_off.wav"

TARDIS:AddPart(PART)